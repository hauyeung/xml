<?xml version="1.0"?>

<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
<xsl:output name="addressbook-format" method="xhtml" indent="yes"
            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
            doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

<xsl:output method="html" indent="yes" name="html"/>

<xsl:template match="/">
<xsl:result-document href="addressbook.html" format="addressbook-format">
  <html>
  <body>
    <h2>Address Book</h2>
    <table>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Home Phone</th>
        <th>Work Phone</th>
        <th>Cell Phone</th>
        <th>Address</th>
        <th>Email</th>
      </tr>
      <xsl:for-each select="People/Person">
        <tr>
          <td><xsl:value-of select="First"/></td>
          <td><xsl:value-of select="Last"/></td>
          <td><xsl:value-of select="HomePhone"/></td>
          <td><xsl:value-of select="WorkPhone"/></td>
          <td><xsl:value-of select="CellPhone"/></td>
          <td>
            <xsl:value-of select="child::Address"/>
          </td>
          <td><xsl:value-of select="Email"/></td>
        </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:result-document>

<xsl:result-document href="addressbook2.html" format="html">
  <html>
  <body>
    <h2>Last Name:</h2>
      <xsl:for-each-group select="People/Person" group-by="@Group">
        Last Name: <xsl:value-of select="current-grouping-key()"/><br /><br />
        
      <xsl:for-each select="current-group()">
       Person: <xsl:value-of select="concat(First, ' ', Last)"/><br />
       Home phone: <xsl:value-of select="HomePhone"/><br />
       Work Phone: <xsl:value-of select="WorkPhone"/><br />
       Cell Phone:   <xsl:value-of select="CellPhone"/><br />
       Address: <xsl:value-of select="child::Address"/><br />
       Email: <xsl:value-of select="Email"/><br />
       <br />
      </xsl:for-each>
      </xsl:for-each-group>
  </body>
  </html>
</xsl:result-document>

</xsl:template>

</xsl:stylesheet>