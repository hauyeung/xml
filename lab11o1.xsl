<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Employees</h2>
    <xsl:value-of select="child::Employees" /><br />
    <xsl:value-of select="Employees/Employee/Phone/attribute::Type" /><br />
    <xsl:value-of select="child::Employees/Employee/attribute::IsManager" /><br />
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>