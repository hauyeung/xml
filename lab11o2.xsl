<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Employees</h2>
      <xsl:for-each select="/Employees/Employee">
       <xsl:value-of select="First"/>&#160;<xsl:value-of select="Last"/>  - <xsl:value-of select="HourlyRate*1.05" /><br />
      </xsl:for-each>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>