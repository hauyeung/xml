<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Employees</h2>
  <table>
    <tr>
      <th>Name</th>
      <th>Birthday</th>
      <th>Hourly Rate</th>
      <th>Phone</th>
    </tr>

    <xsl:for-each select="Employees/Employee">
      <tr>
      <td> <xsl:value-of select="First"/>&#160;<xsl:value-of select="Last"/></td>
      <td><xsl:value-of select="Birthday"/></td>
      <td> $<xsl:value-of select="HourlyRate"/></td>
      <td>
      <xsl:for-each select="Phone">
        <xsl:value-of select="@Type"/> - <xsl:value-of select="//Phone"/><br />
      </xsl:for-each>
      </td>
      </tr>
    </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>