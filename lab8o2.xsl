<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml"/>
<xsl:template match="/">
  <Employees xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="lab8o2.xsd">
    <xsl:apply-templates/>
  </Employees>
</xsl:template>
<xsl:template match="Employees/Employee">
  <Employee>
      <Lastname>
        <xsl:apply-templates select="Last"/>
      </Lastname>
      <Firstname>
        <xsl:apply-templates select="First"/>
      </Firstname>
      <Birthday>
        <xsl:apply-templates select="Birthday"/>
      </Birthday>
      <Wage>
        <xsl:apply-templates  select="HourlyRate"/>
      </Wage>
      <PhoneType>
        <xsl:apply-templates select="@Type"/>
      </PhoneType>
      <Phone>
        <xsl:apply-templates select="Phone"/>
      </Phone>
  </Employee>
</xsl:template>
</xsl:stylesheet>